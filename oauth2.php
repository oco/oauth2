<?php
/**
 * Created by PhpStorm.
 * User: 陈薪名
 * Date: 2018/1/30
 * Time: 10:03
 */
$uConfig = [
    'app_type' => isset($_GET['app_type']) ? $_GET['app_type'] : '',//应用的类型
    'app_id' => isset($_GET['app_id']) ? $_GET['app_id'] : '',//应用的唯一标识
    'scope' => isset($_GET['scope']) ? $_GET['scope'] : '',//应用授权作用域
    'state' => isset($_GET['state']) ? $_GET['state'] : '',//验证变量
    'redirect_uri' => isset($_GET['redirect_uri']) ? $_GET['redirect_uri'] : '',////授权后重定向的回调链接地址， 请使用 urlEncode 对链接进行处理
];//用户参数

$sysConfig = require("config.php");
$oauth2 = new oauth2();
$oauth2->sysConfig = $sysConfig;
$oauth2->uConfig = $uConfig;
$oauth2->init();

class oauth2
{
    /*
     * 用户参数
     */
    public $uConfig;
    /*
     * 系统参数
     */
    public $sysConfig;

    public function init()
    {
        $this->ParamValidate();
        $url = $this->AssembleUrl();
        header("Cache-Control: no-cache");
        header("Pragma: no-cache");
        header("Location:" . $url);
    }

    /*
     *参数验证
     */
    private function ParamValidate()
    {
        if (empty($this->uConfig['app_type'])) $this->ShowErr("应用的app_type不能为空");
        if (empty($this->uConfig['app_id'])) $this->ShowErr("应用的app_id不能为空");
        if (empty($this->uConfig['scope'])) $this->ShowErr("应用的scope不能为空");
        if (empty($this->uConfig['state'])) $this->ShowErr("应用的state不能为空");
        if (empty($this->uConfig['redirect_uri'])) $this->ShowErr("应用的redirect_uri不能为空");
    }

    /*
     *URL组装
     */
    private function AssembleUrl()
    {
        $time = time() + 60 * 6;//到期时间
        if ($this->uConfig['app_type'] == 'weibo') {
            $time = 4070908800;
        }
        $SurplusParams = $this->ToUrlParams();//额外参数
        $this->uConfig['redirect_uri'] = urlencode(implode('', [
            $this->uConfig['redirect_uri'],
            strpos($this->uConfig['redirect_uri'], '?') ? '&' : '?',
            'app_type=' . $this->uConfig['app_type'],
            '&app_id=' . $this->uConfig['app_id'],
            '&state=' . $this->uConfig['state']
        ]));
        /***重组参数****/
        $redirect_uri = urlencode(implode('', [
            $this->sysConfig['sys_redirect_url'],
            strpos($this->sysConfig['sys_redirect_url'], '?') ? '&' : '?',
            'u_url=' . urlencode($this->uConfig['redirect_uri']),
            '&sys_out_time=' . $time
        ]));
        $state = md5($time . $this->sysConfig['sign_key'] . $this->uConfig['redirect_uri']);//签名内容
        //判断授权渠道
        switch ($this->uConfig['app_type']) {
            case 'weixin'://公众号
                $app_url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=' . $this->uConfig['app_id'] . '&redirect_uri=' . $redirect_uri
                    . '&response_type=code&scope=' . $this->uConfig['scope'] . '&state=' . $state . '#wechat_redirect';
                break;
            case 'alipay'://生活号
                $app_url = 'https://openauth.alipay.com/oauth2/publicAppAuthorize.htm?app_id=' . $this->uConfig['app_id'] . '&scope=' . $this->uConfig['scope'] . '&redirect_uri=' . $redirect_uri . '&state=' . $state;
                break;
            case 'qq01'://qq互联 pc
                $app_url = 'https://graph.qq.com/oauth2.0/authorize?client_id=' . $this->uConfig['app_id'] . '&scope=' . $this->uConfig['scope'] . '&redirect_uri=' . $redirect_uri . '&state=' . $state . '&response_type=code' . '&' . $SurplusParams;
                break;
            case 'qq02'://qq互联 WAP网站
                $app_url = 'https://graph.z.qq.com/moc2/authorize?client_id=' . $this->uConfig['app_id'] . '&scope=' . $this->uConfig['scope'] . '&redirect_uri=' . $redirect_uri . '&state=' . $state . '&response_type=code' . '&' . $SurplusParams;
                break;
            case 'weibo'://新浪微博
                $app_url = 'https://api.weibo.com/oauth2/authorize?client_id=' . $this->uConfig['app_id'] . '&response_type=code&redirect_uri=' . $redirect_uri . '&scope=' . $this->uConfig['scope'] . '&state=' . $state . '&' . $SurplusParams;
                break;
            case 'ding01'://钉钉密码登陆
                $app_url = 'https://oapi.dingtalk.com/connect/oauth2/sns_authorize?appid=' . $this->uConfig['app_id'] . '&response_type=code&scope=' . $this->uConfig['scope'] . '&state=' . $state . '&redirect_uri=' . $redirect_uri;
                break;
            case 'ding02'://钉钉扫码登录（跳转）
                $app_url = 'https://oapi.dingtalk.com/connect/qrconnect?appid=' . $this->uConfig['app_id'] . '&response_type=code&scope=' . $this->uConfig['scope'] . '&state=' . $state . '&redirect_uri=' . $redirect_uri;
                break;
            default:
                $app_url = '';
                $this->ShowErr("暂不支持该应用(" . $this->uConfig['app_type'] . ")的跳转");
                break;
        }
        return $app_url;

    }

    /**
     * 格式化参数格式化成url参数
     */
    private function ToUrlParams()
    {
        $arr = array_diff($_GET, $this->uConfig);
        $buff = "";
        foreach ($arr as $k => $v) {
            if ($v != "" && !is_array($v)) {
                $buff .= $k . "=" . $v . "&";
            }
        }
        $buff = trim($buff, "&");
        return $buff;
    }

    /*
     * 输出错误信息
     * */
    private function ShowErr($msg)
    {
        exit(require("error.php"));
    }
}
