<?php
/**
 * Created by PhpStorm.
 * User: 陈薪名
 * Date: 2018/1/30
 * Time: 10:03
 */
//用户参数
$uConfig = [
    'u_url' => isset($_GET['u_url']) ? $_GET['u_url'] : '',//回调地址
    'sys_out_time' => isset($_GET['sys_out_time']) ? $_GET['sys_out_time'] : '',//应用调用时间戳
    'code' => isset($_GET['code']) ? $_GET['code'] : '',//授权值
    'state' => isset($_GET['state']) ? $_GET['state'] : '',//验证变量
];
//支付宝回调时处理
if (empty($uConfig['code'])) {
    $uConfig['code'] = isset($_GET['auth_code']) ? $_GET['auth_code'] : '';//授权值
}

$sysConfig = require("config.php");
$redirect = new redirect();
$redirect->sysConfig = $sysConfig;
$redirect->uConfig = $uConfig;
$redirect->init();

class redirect
{
    /*
     * 系统参数
     */
    public $sysConfig;
    /*
     * 用户参数
     */
    public $uConfig;

    public function init()
    {
        $this->ParamValidate();//验证参数
        $this->StateValidate();
        $url = $this->AssembleUrl();
        header("Cache-Control: no-cache");
        header("Pragma: no-cache");
        header("Location:" . $url);
    }

    /***参数验证***/
    private function ParamValidate()
    {
        if (empty($this->uConfig['u_url'])) $this->ShowErr("应用的用户回调地址不能为空");
        if (empty($this->uConfig['sys_out_time'])) $this->ShowErr("应用的失效时间不能为空");
        if (empty($this->uConfig['code'])) $this->ShowErr("应用的授权值不能为空");
        if (empty($this->uConfig['state'])) $this->ShowErr("应用的验证state不能为空");
    }

    /***安全校验验证***/
    private function StateValidate()
    {
        $md5_state = md5($this->uConfig['sys_out_time'] . $this->sysConfig['sign_key'] . $this->uConfig['u_url']);//签名内容
        if ($md5_state != $this->uConfig['state']) {
            $this->ShowErr("应用的安全校验失败");
        }
        if (time() > $this->uConfig['sys_out_time']) {
            $this->ShowErr("回调超时,已超过有效期,请重新发起授权");
        }
    }

    /***URL组装***/
    private function AssembleUrl()
    {
        $redirect_uri = urldecode($this->uConfig['u_url']);
        $SurplusParams = $this->ToUrlParams();
        $redirect_uri = implode('', [
            $redirect_uri,
            strpos($redirect_uri, '?') ? '&' : '?',
            'code=' . $this->uConfig['code'],
            '&' . $SurplusParams
        ]);
        $redirect_uri = trim($redirect_uri, "&");
        return $redirect_uri;
    }

    /**
     * 格式化参数格式化成url参数
     */
    private function ToUrlParams()
    {
        $arr = array_diff($_GET, $this->uConfig);
        $buff = "";
        foreach ($arr as $k => $v) {
            if ($v != "" && !is_array($v)) {
                $buff .= $k . "=" . $v . "&";
            }
        }
        $buff = trim($buff, "&");
        return $buff;
    }

    /***输出错误信息***/
    private function ShowErr($msg)
    {
        exit(require("error.php"));
    }
}

