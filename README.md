# oauth2授权集成

#### 介绍
oauth2授权集成管理

#### 软件架构
软件架构说明

#### 使用说明

部署前修改config.php内的参数

地址：http://域名/oauth2.php（支持https）

示例地址:<a href="https://oauth.cacm.cc/oauth2.php?app_type=qq01&app_id=101304422&scope=get_user_info&state=123456789&redirect_uri=https%3a%2f%2foauth.cacm.cc%2ftest.php%3ftype%3dtest%26id%3d123" target="_blank">https://oauth.cacm.cc/oauth2.php?app_type=qq01&app_id=101304422&scope=get_user_info&state=123456789&redirect_uri=https%3a%2f%2foauth.cacm.cc%2ftest.php%3ftype%3dtest%26id%3d123</a>

回调地址：http://域名/redirect.php（支持https）

请在对应应用绑定本系统的回调地址，以便继承授权


 **1. 参数app_type** 


<a href="https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140842" target="_blank">weixin(微信公众号)</a>

<a href="https://docs.alipay.com/fw/api/105942" target="_blank">alipay(支付宝生活号)</a>

<a href="http://wiki.connect.qq.com/api列表" target="_blank">qq01(QQ PC版)</a>

<a href="http://wiki.connect.qq.com/api列表" target="_blank">qq02(QQ 移动版)</a>

<a href="https://open.weibo.com/wiki/Connect/login" target="_blank">weibo(新浪微博)</a>

<a href="https://open-doc.dingtalk.com/microapp/serverapi3/sz4hm2" target="_blank">ding01(钉钉密码登陆)</a>

<a href="https://open-doc.dingtalk.com/microapp/serverapi3/mrugr3" target="_blank">ding02(钉钉扫码登录（跳转）)</a>


 **2. 参数app_id** :授权应用id

 **3. 参数scope** :授权作用域

   【 a、微信公众号】

    

        snsapi_base （不弹出授权页面，直接跳转，只能获取用户openid）

        snsapi_userinfo （弹出授权页面，可通过openid拿到昵称、性别、所在地。并且， 即使在未关注的情况下，只要用户授权，也能获取其信息 ）

    

   【 b、支付宝生活号】

    

        auth_base：以auth_base为scope发起的网页授权，是用来获取进入页面的用户的userId的，并且是静默授权并自动跳转到回调页的。用户感知的就是直接进入了回调页（通常是业务页面）。

        auth_user：以auth_user为scope发起的网页授权，是用来获取用户的基本信息的（比如头像、昵称等）。但这种授权需要用户手动同意，用户同意后，就可在授权后获取到该用户的基本信息。

    

   【 c、QQ PC版或QQ 移动版（支持同时多个参数,英文逗号分隔(,)） 】

    

        get_user_info：获取登录用户的昵称、头像、性别

        get_vip_info：获取QQ会员的基本信息

        get_vip_rich_info：获取QQ会员的高级信息

        list_album：获取用户QQ空间相册列表

        upload_pic：上传一张照片到QQ空间相册

        do_like：在用户的空间相册里，创建一个新的个人相册

        list_photo：获取用户QQ空间相册中的照片列表

   

    【d、新浪微博（支持同时多个参数,英文逗号分隔(,)）】

    

        all：请求下列所有scope权限

        email：用户的联系邮箱

        direct_messages_write：私信发送接口

        direct_messages_read：私信读取接口

        invitation_write：邀请发送接口

        friendships_groups_read：好友分组读取接口组

        friendships_groups_write：好友分组写入接口组

        statuses_to_me_read：定向微博读取接口组

        follow_app_official_microblog：关注应用官方微博，该参数不对应具体接口，只需在应用控制台填写官方帐号即可。填写的路径：我的应用-选择自己的应用-应用信息-基本信息-官方运营账号（默认值是应用开发者帐号）


    

    【e:钉钉】

    

        snsapi_login：用于钉钉容器外获取用户授权

    

 **4. 参数state** :重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节

 **5. 参数redirect_uri** :回调地址,任意一个可以公网访问的地址




以下参数非必须，根据对应应用填入

6、参数display（新浪微博专有）：

    【

        default：默认的授权页面，适用于web浏览器。

        mobile：移动终端的授权页面，适用于支持html5的手机。

        wap：wap版授权页面，适用于非智能手机。

        client：客户端版本授权页面，适用于PC桌面应用。

        apponweibo：默认的站内应用授权页，授权后不返回access_token，只刷新站内应用父框架。

    】

7、参数forcelogin（新浪微博专有）：是否强制用户重新登录，true：是，false：否。默认false。

8、参数language（新浪微博专有）：授权页语言，缺省为中文简体版，en为英文版。英文版测试中


#### 遗留问题

1、未进行回调域名验证，可自行加入

2、未记录访问日志

3、未严格的参数验证


如有更好的优化建议请留言或提交优化版本



设计之初是为了解决一个公众号多应用多环境使用问题，希望对你有所帮助